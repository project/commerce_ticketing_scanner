(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {
      window.code = '';

      var commerce_product_id = drupalSettings.commerce_ticketing_scanner.scanner.commerce_product_id;

      var video = document.createElement("video");
      var canvasElement = document.getElementById("canvas");
      var canvas = canvasElement.getContext("2d");
      var loadingMessage = document.getElementById("loadingMessage");
      var outputContainer = document.getElementById("output");
      var outputMessage = document.getElementById("outputMessage");
      var outputData = document.getElementById("outputData");

      var ticketStatus = document.getElementById("ticketStatus");
      var markAsScanned = document.getElementById("markAsScanned");
      var markAsNotScanned = document.getElementById("markAsNotScanned");

      var ticketStatusMessages = document.getElementById("ticketStatusMessages");
      var ticketStatusChangeMessages = document.getElementById("ticketStatusChangeMessages");

      markAsNotScanned.onclick = updateTicketStatusAsNotScanned;
      markAsScanned.onclick = updateTicketStatusAsScanned;
      markAsNotScanned.style.display = 'none';
      markAsScanned.style.display = 'none';

      function drawLine(begin, end, color) {
        canvas.beginPath();
        canvas.moveTo(begin.x, begin.y);
        canvas.lineTo(end.x, end.y);
        canvas.lineWidth = 4;
        canvas.strokeStyle = color;
        canvas.stroke();
      }

      // Use facingMode: environment to attemt to get the front camera on phones
      navigator.mediaDevices.getUserMedia({
        video: {
          facingMode: "environment",
          width: { min: 350, ideal: 350 },
          height: { min: 350, ideal: 350 },
          aspectRatio: { ideal: 1 }
        }
      }).then(function (stream) {
        video.srcObject = stream;
        video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
        video.play();
        requestAnimationFrame(tick);
      });

      function tick() {
        loadingMessage.innerText = "⌛ Loading video..."
        if (video.readyState === video.HAVE_ENOUGH_DATA) {
          loadingMessage.hidden = true;
          canvasElement.hidden = false;
          outputContainer.hidden = false;

          canvasElement.height = video.videoHeight;
          canvasElement.width = video.videoWidth;
          canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
          var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
          var code = jsQR(imageData.data, imageData.width, imageData.height, {
            inversionAttempts: "dontInvert",
          });

          if (code) {
            drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
            drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
            drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
            drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
            outputMessage.hidden = true;
            outputData.parentElement.hidden = false;
            outputData.innerText = code.data;

            if (code.data != window.code) {
              ticketStatus.innerHTML = '';
              ticketStatusChangeMessages.innerHTML = '';
              ticketStatusMessages.classList.remove('status');
              ticketStatusMessages.classList.remove('warning');
              ticketStatusMessages.classList.remove('error');

              window.code = code.data;
              ticketStatus.innerHTML = '⌛ Loading...';
              commerce_product_variation_id = $('#edit-commerce-product-variation').val();
              if (commerce_product_variation_id == "") {
                jQuery.get('/product/' + commerce_product_id + '/scan-tickets/check-all/' + window.code, null, updateTicketStatus);
              } else {
                jQuery.get('/product/' + commerce_product_id + '/scan-tickets/check/' + commerce_product_variation_id + '/' + window.code, null, updateTicketStatus);
              }

              // if () {
              //   updateTicketStatusAsScanned();
              // }
            }
          } else {
            outputMessage.hidden = false;
            outputData.parentElement.hidden = true;
          }
        }
        requestAnimationFrame(tick);
      }

      function updateTicketStatus(json) {
        if (!json || json.error) {
          setMessageType('error');
          markAsScanned.style.display = 'none';
          markAsNotScanned.style.display = 'none';
          if (json.error) {
            ticketStatusMessages.innerText = json.error;
          } else {
            ticketStatusMessages.innerText = "Invalid response from server - try refreshing page.";
          }
          return false;
        }

        mail = json.mail ? '<br><br><b>' + json.email + '</b>' : '';
        if (json.state == 'active') {
          ticketStatusMessages.innerHTML = "Ticket is valid and unused" + mail;
          setMessageType('status');

          markAsScanned.style.display = 'block';
          markAsNotScanned.style.display = 'none';
        } else if (json.state == 'used') {
          ticketStatusMessages.innerHTML = "Ticket is already used" + mail;
          setMessageType('error');

          markAsScanned.style.display = 'none';
          markAsNotScanned.style.display = 'block';

        } else if (json.state == 'canceled') {
          ticketStatusMessages.innerHTML = "Ticket (and/or order) was cancelled - " + mail;
          setMessageType('error');

          markAsScanned.style.display = 'none';
          markAsNotScanned.style.display = 'block';
        }
        else {
          ticketStatusMessages.innerHTML = "Error: " + json.error;

          setMessageType('error');

          markAsScanned.style.display = 'none';
          markAsNotScanned.style.display = 'none';
        }
      }

      function setMessageType(messageType) {
        element = ticketStatusMessages;
        element.classList.remove('messages--error');
        element.classList.remove('messages--info');
        element.classList.remove('messages--status');

        element.classList.add('messages--' + messageType);
      }

      function updateTicketStatusAsScanned() {
        var commerce_product_variation_id = $('#edit-commerce-product-variation').val();
        if (commerce_product_variation_id == "") {
          var url = '/product/' + commerce_product_id + '/scan-tickets/set-no-variation/' + window.code + '/used';
        } else {
          var url = '/product/' + commerce_product_id + '/scan-tickets/set/' + commerce_product_variation_id + '/' + window.code + '/used';
        }

        jQuery.post(url, null, function (response) {
          console.log(response);
          // response = response.json();
          if (response.state && response.state == 'used') {
            ticketStatusMessages.innerHTML = "Ticket now marked as used.";
            setMessageType('info');
            markAsScanned.style.display = 'none';
            markAsNotScanned.style.display = 'block';
          } else {
            ticketStatusMessages.innerHTML = "ERROR MARKING AS USED";
            setMessageType('error');
          }
          window.code = '';
        });
      }

      function updateTicketStatusAsNotScanned() {
        var commerce_product_variation_id = $('#edit-commerce-product-variation').val();
        if (commerce_product_variation_id == "") {
          var url = '/product/' + commerce_product_id + '/scan-tickets/set-no-variation/' + window.code + '/active';
        } else {
          var url = '/product/' + commerce_product_id + '/scan-tickets/set/' + commerce_product_variation_id + '/' + window.code + '/active';
        }
        jQuery.post(url, null, function (response) {
          console.log(response);
          // response = response.json();
          if (response.state && response.state == 'active') {
            ticketStatusMessages.innerHTML = "Ticket now marked as NOT used.";
            setMessageType('info');
            markAsScanned.style.display = 'block';
            markAsNotScanned.style.display = 'none';
          } else {
            ticketStatusMessages.innerHTML = "ERROR MARKING AS NOT USED";
            setMessageType('error');
          }
          window.code = '';
        });
      }

    }
  };

})(jQuery, Drupal, drupalSettings);
