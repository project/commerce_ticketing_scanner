<?php

namespace Drupal\commerce_ticketing_scanner\Form;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class ScannerForm.
 */
class ScannerForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scanner_form';
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Product $commerce_product) {
    $ticketed = false;
    $variations = $commerce_product->getVariations();
    foreach ($variations as $variation) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $variation_type */
      $variation_type = \Drupal\commerce_product\Entity\ProductVariationType::load($variation->bundle());
      if ($variation_type->hasTrait('purchasable_entity_ticket')) {
        $ticketed = true;
        break;
      }
    }
    return AccessResult::allowedIf($ticketed && $account->hasPermission('check ticket validity via scanner'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Product $commerce_product = NULL) {
    $options = [];
    $variations = $commerce_product->getVariations();
    $options[''] = 'Any variation';
    foreach ($variations as $variation) {
      $labels = [];
      $labels[] = $variation->label();
      if ($variation->hasField('field_event_variation_title') && $variation->field_event_variation_title->value) {
        $labels[] = $variation->field_event_variation_title->value;
      }
      $options[$variation->id()] = implode(' - ', $labels);
    }
    $form['commerce_product_variation'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the product variation to check tickets for'),
      '#options' => $options,
      '#weight' => '0',
    ];

    $markup = '<div id="loadingMessage">🎥 Unable to access video stream (please make sure you have given permission for camera or have webcam enabled)</div>
    <canvas id="canvas" width="400" height="400" hidden></canvas>
    <div id="output" hidden>
        <div id="outputMessage">🎥 No QR code in current camera view.</div>
        <div hidden><b>QR code data:</b> <span id="outputData"></span></div>
        <h2 style="margin-bottom: 0px;">' . $commerce_product->label() . '</h2>
        <div id="ticketStatusMessages" class="messages messages--status"><span id="ticketStatus"></span></div>
        <div><button type="button" id="markAsScanned" class="button" style="display: none;">Mark as used</button>
        <button type="button" id="markAsNotScanned" class="button" style="display: none;">Mark as not used</button>
        </div>
        <div id="ticketStatusChangeMessages"></div>
    </div>';

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#allowed_tags' => ['div', 'span', 'canvas', 'h2', 'button'],
      '#attached' => [
        'library' => ['commerce_ticketing_scanner/js_qr', 'commerce_ticketing_scanner/scanner'],
        'drupalSettings' => ['commerce_ticketing_scanner' => ['scanner' => ['commerce_product_id' => $commerce_product->id()]]]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::messenger()->addMessage($key . ': ' . ($key === 'text_format' ? $value['value'] : $value));
    }
  }
}
